FROM node:latest
RUN npm config list -l --json

WORKDIR /src

# App
ADD . /src
# Install app dependencies




RUN npm install


EXPOSE 8080

CMD ["npm", "start"]
